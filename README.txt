BLS12(128-bit Security Lebel)

Curve
	y^2=x^3+4

Mother parameter
	-2^77+2^50+2^33

Prime 
	prime length:461bit

Extension field
	Fp2:f(x)=x^2+1
	Fp6:f(x)=x^3-(1,1)
	Fp12:f(x)=x^2-((0,0),(1,0),(0,0))

Functions
	tate pairing			
	ate pairing(8-sparse)		
	opt-ate pairing(8-sparse)	
	normal G1 SCM				
	2div G1 SCM					
	2div G1 SCM(Joint Sparse Form)
	normal G2 SCM
	2div G2 SCM
	4div G2 SCM
	2div G3 exp
	4div G3 exp
